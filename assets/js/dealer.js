/**
 * dealers_list_action function used for dealers list action
 * @param {type} value
 * @returns {undefined}
 */
function dealers_list_action(value)
{
    if(value==="View Detail")
    {
        window.location.href="dealers-view.html";
    }
    else if(value==="Edit Detail")
    {
        window.location.href="dealers-edit.html";
    }
    else if(value==="View All Cars")
    {
        window.location.href="dealers-cars.html";
    }
    else if(value==="View Activity")
    {
        window.location.href="dealers-activity.html";
    }
    else if(value==="View All Transactions")
    {
        window.location.href="dealers-transactions.html";
    }
    else if(value==="Assign RE")
    {
        window.location.href="cars-pending-fps.html";
    }
}