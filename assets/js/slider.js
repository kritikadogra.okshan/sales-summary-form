(function($) {	
	$( "#mySlider" ).slider({
		range: true,
		min: 100000,
		max: 500000,
		values: [ 200000, 400000 ],
		slide: function( event, ui ) {
			$( "#price" ).val( "  ₹ " + ui.values[ 0 ] + " -   ₹ " + ui.values[ 1 ] );
		}
	});

	$( "#price" ).val( "  ₹ " + $( "#mySlider" ).slider( "values", 0 ) +
			   " -   ₹ " + $( "#mySlider" ).slider( "values", 1 ) );
			   
	$( "#km_driven_slider" ).slider({
		range: true,
		min: 0,
		max: 100000,
		values: [ 1000, 50000 ],
		slide: function( event, ui ) {
			$( "#km_driven" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
		}
	});
	
        
        $( "#km_driven" ).val( "" + $( "#km_driven_slider" ).slider( "values", 0 ) +
			   " - " + $( "#km_driven_slider" ).slider( "values", 1 ) );	
			   
$( "#km_driven_slider1" ).slider({
		range: true,
		min: 0,
		max: 100000,
		values: [ 1000, 50000 ],
		slide: function( event, ui ) {
			$( "#km_driven1" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
		}
	});
	
        
        $( "#km_driven1" ).val( "" + $( "#km_driven_slider1" ).slider( "values", 0 ) +
			   " - " + $( "#km_driven_slider1" ).slider( "values", 1 ) );			   
})(jQuery);