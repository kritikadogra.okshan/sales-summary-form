/**
 * cars_list_action function used for cars list action
 * @param {type} value
 * @returns {undefined}
 */
function cars_list_action(value)
{
    if(value==="View Detail")
    {
        window.location.href="cars-view.html";
    }
    else if(value==="Edit Detail")
    {
        window.location.href="cars-edit.html";
    }
    else if(value==="View Inpection")
    {
        window.location.href="cars-view-inspection.html";
    }
    else if(value==="Edit Inpection")
    {
        window.location.href="cars-edit-inspection.html";
    }
    else if(value==="Approve")
    {
        alert("Are you sure want to approve ?");
    }
    else if(value==="Schedule Auction")
    {
        $("#schedule_auction_model").modal('show');
    }
    else if(value==="Change State")
    {
        $("#change_state_model").modal('show');
    }
    else if(value==="View Live Auction")
    {
        $("#view_live_auction_model").modal("show");
    }
    else if(value==="Add Offline Transaction")
    {
        $("#add_offline_transaction_model").modal('show');
    }
    else if(value==="Assign/Change RE")
    {
        $("#assign_re_model").modal('show');
    }
    else if(value==="Assign to BO")
    {
        $("#assign_bo_model").modal('show');
    }
    else if(value==="Raise Payment Request")
    {
        $("#raise_payment_request_model").modal('show');
    }
    else if(value==="Make Payment Request")
    {
        $("#make_payment_request_model").modal('show');
    }
    else if(value==="Initiate Refund")
    {
        $("#initiate_refund_model").modal('show');
    }
   else if(value==="View Auction Detail")
    {
        window.location.href="cars-live-auction-detail.html";
    }
   else if(value==="Edit Auction Detail")
    {
        window.location.href="cars-edit-auction-detail.html";
    } 
}